/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.nontalee.tictactoe;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author focus
 */
public class UnitTest {

    public UnitTest() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    /**
     *
     */
    @Test
    public void testCheckVerticlePlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
        public void testCheckVerticlePlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
        public void testCheckVerticlePlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
    @Test
        public void testCheckVerticlePlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'X', '-', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
      @Test  
        public void testCheckVerticlePlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
       @Test 
        public void testCheckVerticlePlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', 'X'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OXProgram.checkVertical(table, currentPlayer, col));
    }
        @Test
        public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }

        @Test
        public void testCheckHorizontalPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int row = 1;
        assertEquals(true, OXProgram.checkHorizontal(table, currentPlayer, row));
    }
       
        @Test
        public void testCheckPlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
    }
        @Test
        public void testCheckPlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'O', 'O', 'O'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col = 2;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
         @Test
        public void testCheckPlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'X', '-', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        int row = 1;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', 'X'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        int row = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXRow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 3;
        int row = 1;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXRow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', 'X'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        int row = 2;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXRow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 3;
        assertEquals(true, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerONoWin() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'O', 'O', '-'}};
        char currentPlayer = 'X';
        int col = 3;
        int row = 3;
        assertEquals(false, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerXNoWin() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'X', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        int row = 3;
        assertEquals(false, OXProgram.checkWin(table, currentPlayer, col, row));
        }
        @Test
        public void testCheckPlayerOCrossX1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
        }
        @Test
        public void testCheckPlayerOCrossX2Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', 'O', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
        }
        @Test
        public void testCheckPlayerXCrossX1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX1(table, currentPlayer));
        }
        @Test
        public void testCheckPlayerXCrossX2Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', 'X', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        assertEquals(true, OXProgram.checkX2(table, currentPlayer));
        }
        @Test
        public void testCheckDraw() {
        char table[][] = {{'O', 'O', 'X'}, 
                                 {'O', 'X', 'O'}, 
                                 {'X', 'X', 'O'}};
        int count = 8;
        assertEquals(true, OXProgram.checkDraw(count));
        } 
    }
